
<!-- README.md is generated from README.Rmd. Please edit that file -->

# guinnr

<!-- badges: start -->

<!-- badges: end -->

The goal of guinnr is to provide an easy, consistent, reproducible way
to apply Guinn Center brand standards (colors, fonts, etc.) to figures
and tables developed in `R`.

## Installation

### Dependencies

The Guinn Center style guide stipulates the use of the PT Sans and PT
Serif font families. These can be installed from google fonts: [PT
Serif](https://fonts.google.com/specimen/PT+Serif); [PT
Sans](https://fonts.google.com/specimen/PT+Sans).

### Installation Instructions

You can install the released version of guinnr from
[gitlab](https://gitlab.com/dliden/guinnr) with:

``` r
install_gitlab("dliden/guinnr")
```

## Example

One problem that this package solves is installation of fonts. Many
systems do not come pre-installed with the PT sans and PT serif font
families. These fonts are included in the package and can easily be
installed for use in R.

``` r
library(guinnr)
guinnr::load_fonts()

plot(mtcars$wt, mtcars$mpg, family="PT sans", main = "MPG vs. Weight",
     xlab = "mpg", ylab="weight")
```

<img src="man/figures/README-example-1.png" width="100%" />
